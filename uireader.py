
import random
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import QTimer
import pyqtgraph as pg

class ShowForm:
    def __init__(self, inform):
        self.mainui = inform
        self.mainui.show()

class MyChart:
    def __init__(self, inform):
        self.mainui = inform
        self.plot_widget = pg.PlotWidget()
        self.mainui.qvboxChart.addWidget(self.plot_widget)

        self.power = [1]                #starting number of trend flactuate hand
        print(self.power[0])
        self.power_factors_resistance = [1]
        self.power_factors_support = [0]

        self.timer = QTimer()
        self.timer.timeout.connect(self.showchart)
        self.timer.start(1000)     #speed of timer or trend

        self.showchart()

    def showchart(self):
        newtrend = random.uniform(0,1)              #temporary random to  give range for
                                                    # horizontal number or
                                                    # you can remove this if you have a database

        self.power.append(newtrend)                 # tempoary append from random number
                                                    # but it sould based from database ast record

        self.power_factors_resistance.append(1)
        self.power_factors_support.append(0)

        we = len(self.power) - 1
        print("Last: ", self.power[we])
        print("curent: ", self.power)

        if self.power[we] >= 0.8:
            self.plot_widget.plot(self.power, pen='g', symbol='o', symbolSize='0', symbolBrush='w', name='green')
        else:
            self.plot_widget.plot(self.power, pen='b', symbol='o', symbolSize='0', symbolBrush='w', name='blue')

        if len(self.power) == 15:
            self.plot_widget.clear()
            self.power = [self.power[we]]
            self.power_factors_resistance = [1]             # refresh number count of green line
            self.power_factors_support = [0]                # # refresh number count of blue line

        self.plot_widget.plot(self.power_factors_resistance, pen='g', symbol='x', symbolSize='0', symbolBrush='y', name='resistance')
        self.plot_widget.plot(self.power_factors_support, pen='b', symbol='x', symbolSize='0', symbolBrush='y', name='resistance')

if __name__ == '__main__':
    app = QApplication([])
    uiapp = loadUi('widchart.ui')
    main = ShowForm(uiapp)
    chart = MyChart(uiapp)
    app.exec()